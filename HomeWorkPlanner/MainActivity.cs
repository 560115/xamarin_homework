﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace HomeWorkPlanner
{
	[Activity (Label = "Home Work Tracker", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		int count = 1;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			//Ensure that the local database has indeed been created
			string dbCreatedResult = Helper.checkLocalDBCreated ();
			if ( ! string.IsNullOrEmpty(dbCreatedResult))
				createInformationAlert(dbCreatedResult);


			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);
			
			button.Click += delegate {
				button.Text = string.Format ("{0} clicks!", count++);
			};
		}

		void createInformationAlert(string title)
		{
			//set alert for executing the task
			AlertDialog.Builder alert = new AlertDialog.Builder (this);

			alert.SetTitle (title);

			alert.SetPositiveButton ("OK", (senderAlert, args) => {
				//change value write your own set of instructions
				//you can also create an event for the same in xamarin
				//instead of writing things here
			} );

			//alert.SetNegativeButton ("Not doing great", (senderAlert, args) => {
				//perform your own task for this conditional button click
			//} );
			//run the alert in UI thread to display in the screen
			RunOnUiThread (() => {
				alert.Show();
			} );
		}


	}
}


