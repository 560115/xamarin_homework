﻿using System;
using System.IO;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using SQLite;

namespace HomeWorkPlanner
{
	public static class Helper
	{
		private static System.Environment.SpecialFolder personalFolder = System.Environment.SpecialFolder.Personal;
		private static string personalFolderPath = System.Environment.GetFolderPath(personalFolder);
		private static string databasename = "HomeWorkLog.db3";
		private static string fulldatabaseName = string.Empty;

		/// <summary>
		/// Checks the local DB created. If not autocreate local database using the passed in database name
		/// </summary>
		/// <returns><c>true</c>, if local DB created was checked, <c>false</c> otherwise.</returns>
		/// <param name="databasename">Databasename.</param>
		public static string checkLocalDBCreated()
		{
			return createLocalDatabase ("Android", false).ToLower ();
		}





		#region private methods
			

				private static string createLocalDatabase(string platform, bool deleteExisting)
			{
				try
				{    
					if (platform.ToUpper () == "ANDROID") {
					// Just use whatever directory SpecialFolder.Personal returns
					fulldatabaseName = Path.Combine(personalFolderPath, databasename);
				}
				else
				{
					// we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
					// (they don't want non-user-generated data in Documents)
					string documentsPath = personalFolderPath; // Documents folder
					string libraryPath = Path.Combine (documentsPath, "..", "Library"); // Library folder instead

					fulldatabaseName = Path.Combine (libraryPath, databasename);
				}

				if ( deleteExisting)
				{
					if ( File.Exists(fulldatabaseName))
					{
						File.Delete(fulldatabaseName);
					}
				}
				
				//if the database file does not exist then create the database with the default table
				if ( !File.Exists(fulldatabaseName))
				{
					using (var conn= new SQLite.SQLiteConnection(fulldatabaseName))
					{
						conn.CreateTable<HomeWork>();
					}

					return "Database created";
				}

				return string.Empty;

			}
			catch(Exception exCreatingDatabase) {
				return string.Format ("Error: {0}", exCreatingDatabase.Message);
				}

			}




		#endregion 
	}
}

