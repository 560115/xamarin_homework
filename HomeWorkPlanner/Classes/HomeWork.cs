﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using SQLite;

namespace HomeWorkPlanner
{
	public class HomeWork
	{

		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Teacher { get; set; }
		public string Subject { get; set; }
		public string GivenDate { get; set; }
		public string Task { get; set; }
		public string HandInDate { get; set; }
		public bool completed { get; set;}
		public bool completedDate { get; set;}
		public string emailReminderDateSent { get; set;}
		public string emailCompleteDateSent { get; set;}
		public bool duplicateHomeWorkIgnore {get; set;}

	}

}

